package ru.kev.searchtext;

import java.awt.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Класс для поиска строки, введённой пользователем, в текстовых файлах указанной директории,
 * а также в поддиректориях этой папки.
 *
 * @author Kotelnikova E.V. group 15it20
 */

public class ReadDirectory {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        //pathDirectory = new File("D:\\Work\\textFiles");
        File pathDirectory = new File(inputPath());
        System.out.println("Введите строку для поиска в текстовых файлах: ");
        String searchString = scanner.next();
        String[] listDir = pathDirectory.list();
        System.out.println("Список объектов, хранящихся в директории: " + Arrays.toString(listDir));

        File[] files = pathDirectory.listFiles();
        assert files != null;

        BufferedWriter writer = new BufferedWriter(new FileWriter("nameOfFiles.txt"));


        ArrayList<File> textFiles = new ArrayList<>();
        for (File file : files) {
            if (file.isFile()) {
                if (findTextFiles(file)) {
                    textFiles.add(file);
                }
            }
            if (file.isDirectory()) {
                String[] listSubdir = pathDirectory.list();
                System.out.println("Список объектов, хранящихся в поддиректории " + file.getName() + ": " + Arrays.toString(listSubdir));
                File[] filesInSubDirectory = file.listFiles();
                assert filesInSubDirectory != null;

                for (File tmpfile : filesInSubDirectory) {
                    if (findTextFiles(tmpfile)) {
                        textFiles.add(tmpfile);
                    }
                }
            }
        }

        for (File file : textFiles) {
            print(findText(file, searchString), writer);
        }
        writer.close();
    }

    /**
     * Метод для записи результирующх строк в файл и вывода их в консоль
     *
     * @param strings список, содержающий результирующие строки для вывода
     * @param writer  объект BufferedWriter
     * @throws IOException исключения ввода-вывода
     */
    public static void print(ArrayList<String> strings, BufferedWriter writer) throws IOException {
        for (String str : strings) {
            writer.write(str);
            System.out.println(str);
        }
    }

    /**
     * Метод с помощью построчного считывания строк из файла проверяет текст на наличие определенной фразы
     *
     * @param file         текстовый файл
     * @param searchString строка, которую нужно найти в текстовом файле
     * @throws IOException исключение ввода-вывода
     */

    public static ArrayList<String> findText(File file, String searchString) throws IOException {
        ArrayList<String> strings = new ArrayList<>();
        String s;
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            int countOfString = 0;
            while ((s = reader.readLine()) != null) {
                countOfString++;
                if (s.contains(searchString)) {
                    strings.add("В файле " + file.getName() + " данный текст найден в строке: " + countOfString + "\n");
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("File not found");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return strings;
    }

    /**
     * Метод проверяет, является ли файл текстовым
     *
     * @param file файл, хранящийся в директории
     * @return true, если файл является текстовым, иначе false
     */

    public static boolean findTextFiles(File file) {
        if (file.isFile()) {
            System.out.println("Файл, хранящийся в директории: " + file.getName());
            if (file.getName().contains(".txt")) {
                return true;
            }
        }
        return false;
    }


    /**
     * Метод позволяет ввести путь к стартовому каталогу, учитывая корректность ввода данных
     *
     * @return строка, содержащая введенный путь
     */

    public static String inputPath() {
        System.out.println("Введите путь к файлу: ");
        String way = scanner.next();
        Path path;
        try {
            path = Paths.get(way);
            if (Files.exists(path)) {
                System.out.println("Путь введён верно!");
            }
        } catch (InvalidPathException e) {
            System.out.println("Введён неправильный путь. ");
        }
        return way;
    }
}

